# Blog Posts 


## Description
This repository contains all of my blog posts written in markdown format concerning various cybersecurity/it topics.

## Purpose
This repository simply serves as a backup for any adverse event which may occur to the blogging platform.

## Contents
- **Posts (2023)**
  - Zero Trust Architechture
  - Software and Firmware Vulnerabilities
  - Basic Binary Disassembly (using Radare2)
  - Firmware Reversing
---
- **Posts (2024)**
  - Active Directory Domain Service

## Author
tcincere
