## Defining AD DS
	- The database is central store of all domain objects (user accounts, computer accounts and groups).
	- AD DS can:
		- Install, configure and update apps.
		- Manage security infrastructure.
		- Enable remote and direct access services
		- Manage digital certificates.
	- ### Logical Components
		- 1. **Partition**: Sections of the AD DS database containing different data types (e.g., schema, configuration, domain data). Stored on multiple domain controllers and updated through replication.
		  2. **Schema**: Definitions of object types and attributes in AD DS.
		  3. **Domain**: Logical administrative container for objects like users and computers, organized hierarchically in parent-child relationships.
		  4. **Domain tree**: Hierarchical collection of domains with a common root domain and DNS namespace.
		  5. **Forest**: Collection of one or more domains with a common AD DS root, schema, and global catalog.
		  6. **OU (Organizational Unit)**: Container for users, groups, and computers, used for delegating administrative rights and linking Group Policy Objects (GPOs).
		  7. **Container**: Organizational framework objects in AD DS; default or custom, but GPOs cannot be linked to them.
	- ### Physical Components
		- 1. **Domain controller**: Contains a copy of the AD DS database; processes changes and replicates them to other domain controllers.
		  2. **Data store**: Exists on each domain controller, using Microsoft Jet database technology to store directory information in the Ntds.dit file and associated log files.
		  3. **Global catalog server**: Domain controller hosting a read-only copy of all objects in a multi-domain forest, speeding up cross-domain searches.
		  4. **Read-only domain controller (RODC)**: Special, read-only AD DS installation for locations with less physical security or IT support.
		  5. **Site**: Container for AD DS objects specific to a physical location, as opposed to logical structure.
		  6. **Subnet**: Portion of the network's IP addresses assigned to computers in a site; a site can contain multiple subnets.
- ## Users, groups and computers
	- You can use these to create and manage user objects:
	  collapsed:: true
		- Active Directory Administrative Center.
		- Active Directory Users and Computers.
		- Windows Admin Center.
		- Windows PowerShell.
		- The `dsadd` command-line tool.
	- ### Managed Service Accounts
		- Managed service accounts are a type of Active Directory object designed to facilitate the management of service accounts for applications. These accounts address the challenges associated with using domain-based service accounts (used to centralise administration) by providing a managed service account which enables:
		  collapsed:: true
			- **Simplified password management**: Automates and secures the management of service account passwords.
			- **Simplified Service Principal Name (SPN) management**: Eases the administrative burden of managing SPNs.
			- By using managed service accounts, organizations can centralize administration and meet program requirements more efficiently, reducing the extra effort typically needed to manage domain-based service accounts.
		- ### Group managed service accounts
		  collapsed:: true
			- gMSAs extend the capabilities of standard managed service accounts to multiple servers within a domain. They are particularly useful in server farm scenarios with Network Load Balancing (NLB) clusters or IIS servers where the same service account needs to be used across several servers. gMSAs offer benefits like automatic password maintenance and simplified SPN management for services running on multiple servers.
			- To use gMSAs, you must create a KDS root key on a domain controller in your domain.
				- ``Add-KdsRootKey –EffectiveImmediately``
				- You create group managed service accounts by using `New-ADServiceAccount` Windows PowerShell cmdlet with the `–PrinicipalsAllowedToRetrieveManagedPassword` parameter.
					- ``New-ADServiceAccount -Name LondonSQLFarm -PrincipalsAllowedToRetrieveManagedPassword SEA-SQL1, SEA-SQL2, SEA-SQL3``
		- ### Group objects
		  collapsed:: true
			- In small networks, assigning permissions to individual user accounts may be practical. However, in large enterprise networks, this approach is inefficient. Instead, it's more effective to create groups containing the necessary user accounts and assign permissions to these groups, streamlining the management of access levels.
			- **Group type:**
				- **Security**: Used to assign permissions and control resource access in ACLs. Necessary for managing security. If you want to use a group to manage security, it must be a security group
				- **Distribution**: Used for email applications and not security-enabled. Security groups can also serve distribution purposes.
		- ### Group scopes
		  collapsed:: true
			- Group scope determines the groups abilities/permissions and the group membership. Four scopes are:
				- **Local**: Used for standalone servers, workstations, or domain-member servers/workstations. Abilities and permissions are for local resources only. Members can be from anywhere in the AD DS forest.
				- **Domain-local**: Primarily used for resource management and access assignment. Exists on domain controllers in an AD DS domain. Abilities and permissions are for domain-local resources. Members can be from anywhere in the forest.
				- **Global**: Consolidates users with similar characteristics (e.g., department or location). Abilities and permissions can be assigned anywhere in the forest. Members are from the local domain only and can include users, computers, and global groups from the local domain.
				- **Universal**: Used in multidomain networks, combining characteristics of domain-local and global groups. Abilities and permissions can be assigned anywhere in the forest. Members can be from anywhere in the AD DS forest.
		- ### Computers
		  collapsed:: true
			- Computers, like users, are considered security principals because:
				- They have an account with a sign-in name and password, automatically changed by Windows periodically.
				- They authenticate with the domain.
				- They can belong to groups and access resources, configurable via Group Policy.
				- The lifecycle of a computer account involves creating the object and joining it to the domain. Day-to-day administrative tasks include configuring properties, moving between OUs, managing the computer, and actions like renaming, resetting, disabling, enabling, and eventually deleting the computer object.
			- #### Computer Containers
				- The Computers container in AD DS is a default location for computer accounts when they join the domain. It's not an OU but rather a Container object with the common name "CN=Computers". Unlike OUs, you can't create sub-OUs within it, nor can you link Group Policy Objects to it. It's recommended to use custom OUs for hosting computer objects instead of relying on the limitations of the Computers container.
- ## Forests and domains
	- ### Defining forests
		- An AD DS forest is a collection of  AD DS trees that contain AD DS domains. Domains in a forest share:
		  collapsed:: true
			- A common root.
			- A common schema.
			- A global catalog
		- An AD DS domain is a logical administrative container for objects such as:
		  collapsed:: true
			- Users
			- Groups
			- Computers
		- A forest in AD DS is a top-level container comprising one or more domain trees sharing a common directory schema and a global catalog. Each domain tree consists of one or more domains with a contiguous namespace. The forest root domain, the first domain created in the forest, contains unique objects. A forest can range from a single domain with one domain controller to multiple domains across several domain trees. For instance, Contoso.com is shown as the forest root domain, with Adatum.com in a separate tree and Seattle.Contoso.com as a child domain of Contoso.com. The following objects exist in the forest root domain:
		  collapsed:: true
			- The schema master role.
			- The domain naming master role.
			- The Enterprise Admins group.
			- The Schema Admins group.
		- An AD DS forest serves as both a security and replication boundary:
		  collapsed:: true
			- 1. **Security Boundary**: By default, users from outside the forest cannot access resources within it. All domains within the forest automatically trust each other.
			- 2. **Replication Boundary**: The forest is the replication boundary for the configuration, schema, and global catalog partitions in the AD DS database. Organizations requiring applications with incompatible schemas must deploy additional forests. The global catalog enables finding objects from any domain within the forest.
			- The following objects exist in each domain (including the forest root):
				- The RID master role.
				- The Infrastructure master role.
				- The PDC emulator master role.
				- The Domain Admins group.
	- ### Defining domains
	  collapsed:: true
		- An AD DS domain serves as a logical container for managing user, computer, group, and other objects. The AD DS database stores all domain objects, with each domain controller maintaining a copy.
		- Also described as:
			- 1. **Replication Boundary**: Changes made in the domain replicate across all domain controllers within it, with a multi-master replication model allowing changes from any controller.
			- 2. **Administrative Unit**: It includes an Administrator account and a Domain Admins group, granting full control over domain objects by default.
		- An AD DS domain provides:
			- 1. **Authentication**: It verifies the identity of domain-joined computers and users during computer startup or user sign-in by confirming their credentials against AD DS.
			- 2. **Authorization**: Through access control technologies, it determines whether authenticated users have permission to access resources.
	- ### Trust relationships
		- AD DS trusts facilitate access to resources in a complex environment. In a single domain setup, granting access is straightforward. However, in multi-domain or forest setups, ensuring appropriate trusts is crucial for consistent resource access.
		- In a multiple-domain forest, two-way transitive trust relationships are automatically generated between AD DS domains, establishing a path of trust between all domains.
		- #### Trust types
			- **Parent and Child Trust:**
				- **Description:** Created when adding a new AD DS domain to an existing tree.
				- **Direction:** Two-way
				- **Transitivity:** Transitive
			- **Tree-root Trust:**
				- **Description:** Automatically created when adding a new AD DS tree to an existing forest.
				- **Direction:** Two-way
				- **Transitivity:** Transitive
			- **External Trust:**
				- **Description:** Enables resource access with Windows NT 4.0 domain or AD DS domain in another forest.
				- **Direction:** One-way or two-way
				- **Transitivity:** Nontransitive
			- **Realm Trust:**
				- **Description:** Establishes authentication path between Windows Server AD DS domain and Kerberos v5 realm.
				- **Direction:** One-way or two-way
				- **Transitivity:** Transitive or nontransitive
			- **Forest Trust:**
				- **Description:** Allows two AD DS forests to share resources.
				- **Direction:** One-way or two-way
				- **Transitivity:** Transitive
			- **Shortcut Trust:**
				- **Description:** Reduces authentication time between AD DS domains in different parts of a forest.
				- **Direction:** One-way or two-way
				- **Transitivity:** Nontransitive
- ## Organisational units
	- An Organizational Unit (OU) is a container object in a domain used to consolidate users, computers, groups, and other objects. GPOs can be linked directly to an OU to manage the contained users and computers. Additionally, an OU manager can be assigned, and a COM+ partition can be associated with an OU.
	- ### Why use OUs?
		- 1. **Consolidation for Management**: OUs allow for grouping objects, simplifying management by applying GPOs to all objects within the OU.
		- 2. **Delegation of Administrative Control**: OUs enable delegation of control over objects, allowing management permissions to be assigned to specific users or groups within AD DS.
		- OUs represent hierarchical and logical structures within an organization, such as departments or geographic regions, facilitating the management of user, group, and computer accounts based on the organizational model.
	- ### Generic containers
		- AD DS includes built-in containers like Users and Computers, which store system objects or serve as default parent objects for new ones. These containers are distinct from OUs in terms of management capabilities. Unlike OUs, containers have limited management capabilities, such as the inability to directly apply a GPO to them.