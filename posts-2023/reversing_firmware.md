## Introduction
collapsed:: true
	- This month we will be looking at reversing firmware; and more specifically, completing the challenge on TryHackMe called `Dumping Router Firmware`.  To follow along, complete the workstation setup required (listed in Task 1: Preparation). The link to the room is [here](https://tryhackme.com/room/rfirmware).
	- *Note:* I will refer to each question by its appearance as a number. However, I regard questions only as those which require an answer from the user.
- ## Investigating firmware
  collapsed:: true
	- By following the steps, you should have a file named `FW_WRT1900ACSV2_2.0.3.201002_prod.img` with an equivalent hash:`dbbc9e8673149e79b7fd39482ea95db78bdb585c3fa3613e4f84ca0abcea68a4`.
	- **Task 1: Question 1 [What does the first clear text line say when running strings on the file?]**
	  collapsed:: true
		- To answer this question, we need to use the `strings` command. We understand that we need to look at the top of the file, so we can pipe this output into `head` which will only output the first 10 lines. `strings FW_WRT1900ACSV2_2.0.3.201002_prod.img | head`
		  ```bash
		  Linksys WRT1900ACS Router
		  @ #!
		  !1C "
		   -- System halted
		  Attempting division by 0!
		  Uncompressing Linux...
		  decompressor returned an error
		   done, booting the kernel.
		  invalid distance too far back
		  invalid distance code
		  ```
		  Here, we can see the name of the router. This answers the first question.
	- **Task 1: Question 2 [What operating system is the device running?]**
	  collapsed:: true
		- This question can be answered through output from the first command. We see that the device is running Linux via `Uncompressing Linux...`. This most likely is using `squashfs` which is a read-only, compressed file system, typically used on embedded devices for its fast access and low storage space needs.
	- **Task 1: Question 3 [What option within Binwalk extracts files from the firmware image?]**
	  collapsed:: true
		- This question can be easily answered by reading the man/help page, using `binwalk --help | grep -i extract`. This command will output any matching lines (case insensitive) which include the word `extract`. Here we can see the option:
		  ```bash
		  Extraction Options:
		      -e, --extract                Automatically extract known file types
		      -D, --dd=<type[:ext[:cmd]]>  Extract <type> signatures (regular expression), give the files an extension of <ext>, and execute <cmd>
		  ```
	- **Task 1: Question 4 [What was the first item extracted?]**
	  collapsed:: true
		- Using the command `binwalk -e FW_WRT1900ACSV2_2.0.3.201002_prod.img`, we can see that the first item extracted was the image header of 64 bytes.
		  ```bash
		  DECIMAL       HEXADECIMAL     DESCRIPTION
		  --------------------------------------------------------------------------------
		  0             0x0             uImage header, header size: 64 bytes, header CRC: 0xFF40CAEC, created: 2020-04-22 11:07:26, image size: 4229755 bytes, Data Address: 0x8000, Entry Point: 0x8000, data CRC: 0xABEBC439, OS: Linux, CPU: ARM, image type: OS Kern
		  ```
		  This also tells us the architecture of the device (ARM).
		-
		-
	- **Task 1: Question 5 [What was the creation date?]**
	  collapsed:: true
		- We can see from the output of the binwalk extract command the creation date, on the first line.
	- **Task 1: Question 6, 7 & 8 [What is the CRC of the image? etc]**
	  collapsed:: true
		- Using information from question four, we can obtain answers to the other questions.
		  ```bash
		  DECIMAL       HEXADECIMAL     DESCRIPTION
		  --------------------------------------------------------------------------------
		  0             0x0             uImage header, header size: 64 bytes, header CRC: 0xFF40CAEC, created: 2020-04-22 11:07:26, image size: 4229755 bytes, Data Address: 0x8000, Entry Point: 0x8000, data CRC: 0xABEBC439, OS: Linux, CPU: ARM, image type: OS Kernel Image, compression type: none, image name: "Linksys WRT1900ACS Router"
		  64            0x40            Linux kernel ARM boot executable zImage (little-endian)
		  26736         0x6870          gzip compressed data, maximum compression, from Unix, last modified: 1970-01-01 00:00:00 (null date)
		  4214256       0x404DF0        Flattened device tree, size: 15563 bytes, version: 17
		  6291456       0x600000        JFFS2 filesystem, little endian
		  ```
		  We can see that the file the CRC , the architecture and the file system is listed. The filesystem is listed as JFFS2, instead of squashfs. This file system is specifically used for NAND flash memory devices (which are commonly found on embedded devices). However, UBIFS (Unsorted Block Image Filesystem is the successor to this file system.)
	- **Task 1: Question 9 [Is that true? (is the device using ARM architecture?)]**
	  collapsed:: true
		- To see if the architechture is correct, we can run `strings 6870 | head -5`. This will list the first five readable strings. We can see that the ARM v7 processor is listed.
		  ```bash
		  Q 0X
		  Q!PU
		  PFV8@-
		  ARMv7 Processor
		  Tainted:
		  
		  ```
	- **Task 1: Question 10 [What is the Linux kernel version?]**
	  collapsed:: true
		- We can run `binwalk -e` on the file `6780`, which is listed as only having `data` when running the file command on it. However, when extracting this file, we obtain the Linux kernel version:
		  ```bash
		  DECIMAL       HEXADECIMAL     DESCRIPTION
		  --------------------------------------------------------------------------------
		  1904228       0x1D0E64        SHA256 hash constants, little endian
		  4112676       0x3EC124        SHA256 hash constants, little endian
		  5877920       0x59B0A0        Linux kernel version 3.10.3
		  6120324       0x5D6384        AES S-Box
		  6120580       0x5D6484        AES Inverse S-Box
		  6176102       0x5E3D66        Unix path: /var/run/rpcbind.sock
		  6261498       0x5F8AFA        MPEG transport stream data
		  6261758       0x5F8BFE        MPEG transport stream data
		  6902132       0x695174        Unix path: /dev/vc/0
		  6993884       0x6AB7DC        xz compressed data
		  7027944       0x6B3CE8        Unix path: /lib/firmware/updates/3.10.39
		  ```
		  Here, I tried to input 3.10.30 but it came back incorrect. Further down, there is another version which proved to be correct.
	- Talk about cpio file:
	- An ASCII cpio archive is a specific format of a cpio archive that stores files in a human-readable ASCII representation. The cpio (copy in, copy out) command is a utility commonly found in Unix-like operating systems for creating and extracting archives.
	- In an ASCII cpio archive, each file entry is represented in plain text, making it human-readable. This format is not as space-efficient as the binary format used by default in cpio, but it allows users to inspect the contents of the archive without specialized tools.
- ## Analysis of the router's filesystem
  collapsed:: true
	- In order to perform analysis of the filesystem, we first need to mount it. Follow the directions provided in the room.
	- **Task 2: Question 1 [Where does linuxrc link to?]**
		- Here we can perform an `ls -la` to list the directories and files within the root directory of the flash filesystem. Taking a look, we can see that linuxrc is symbolically linked to `bin/busybox`. The busybox binary contains common UNIX utilities into a small executable; thus each tool is a minimalist replacement for core utilities, specifically for Linux embedded systems [1](https://linux.die.net/man/1/busybox).
		  ```bash
		  lrwxrwxrwx  1 root root   11 Apr 22  2020 linuxrc -> bin/busybox
		  ```
	- **Task 2: Question 2 [What parent folder to mnt, opt, and var link to?]**
	  collapsed:: true
		- Using the output from the last command, we can see that the parent folder linked is `tmp/`. 
		  ```bash
		  lrwxrwxrwx  1 root root    8 Apr 22  2020 mnt -> /tmp/mnt
		  -r--r--r--  1 root root   20 Apr 22  2020 .mtoolsrc
		  lrwxrwxrwx  1 root root    8 Apr 22  2020 opt -> /tmp/opt
		  lrwxrwxrwx  1 root root    8 Apr 22  2020 var -> /tmp/var
		  ```
	- **Task 2: Question 3 [What folder would store the HTTP server?]**
	  collapsed:: true
		- Again, using the same output, we can see a  file named `www`. Typically, the directory structure follows `/var/www/html`, however this difference may be due to the fact we are running Linux kernel version 3.
		  ```bash
		  drwxr-xr-x  2 root root    0 Apr 22  2020 www
		  ```
	- **Task 2: Question 4 [Where do most of the files link to?]**
	  collapsed:: true
		- Most of the files link to the `busybox` binary, as seen in this snippet.
		  ```bash
		  kali@kali:/mnt/jffs2_file/bin$ ls -la
		  total 1357
		  drwxr-xr-x  2 root root      0 Apr 22  2020 .
		  drwxr-xr-x 17 root root      0 Jan  1  1970 ..
		  lrwxrwxrwx  1 root root      7 Apr 22  2020 addgroup -> busybox
		  lrwxrwxrwx  1 root root      7 Apr 22  2020 adduser -> busybox
		  lrwxrwxrwx  1 root root      7 Apr 22  2020 ash -> busybox
		  -rwxr-xr-x  1 root root   7112 Apr 22  2020 attr
		  -rwxr-xr-x  1 root root 593280 Apr 22  2020 busybox
		  lrwxrwxrwx  1 root root      7 Apr 22  2020 cat -> busybox
		  lrwxrwxrwx  1 root root      7 Apr 22  2020 catv -> busybox
		  ```
	- **Task 2: Question 5 [What database would be running in`/bin`, if it was online?]**
	  collapsed:: true
		- Performing an `ls -la` within `/bin`, we can see the database most likely to be running, while the router is online.
		  ```bash
		  -rwxr-xr-x  1 root root  33764 Apr 22  2020 sqlite3
		  ```
		-
	- **Task 2: Question 6 [What is the build date?]**
	  collapsed:: true
		- We can perform `ls -la` on `/etc/` and see then `builddate` file. We can then run `cat /etc/builddate`.
	- **Task 2: Question 7 [What SSH server does the machine run on?]**
	  collapsed:: true
		- Performing an `ls -la` on `/etc` we can see: 
		  ```bash
		  -r--r--r--  1 root root    458 Apr 22  2020 dropbear_dss_host_key
		  -r--r--r--  1 root root    427 Apr 22  2020 dropbear_rsa_host_key
		  ```
		  These look like SSH keys. Performing a quick internet search, we can see that `dropbear`is a SSH package used as a replacement for OpenSSH, specifically designed for devices like embedded systems [2](https://en.wikipedia.org/wiki/Dropbear_(software)).
	- **Task 2: Question 8 [What company developed media server?]**
	  collapsed:: true
		- We can see the file from `ls -la` named `mediaserver.ini`. From the first line of the file, we can see the company that created that. `head mediaserver.ini`
		  ```bash
		  #! Cisco MediaServer ini file ( twonky revision ) / charset UTF-8
		  #! change settings by editing this file
		  #! version 5.1.05
		  #
		  # the following parameters MUST be configured
		  ```
	- **Task 2: Question 9 [Which files contains a list of services and port numbers?]**
	  collapsed:: true
		- Simply find the file `services` in the `/etc`
	- **Task 2: Question 10 [Which file contains the default system settings?]**
	  collapsed:: true
		- Simply find the file `system_defaults` in the `/etc`.
	- **Task 2: Question 11 [What is the specific firmware version?]**
	  collapsed:: true
		- Simply find the file `version` in the `/etc`.
	- **Task 2: Question 12 [What three networks have a folder?]**
	  collapsed:: true
		- We can run `ls -d */`on `JNAP/modules` and you will see the three directories listed. The `ls` command simply lists only the directories and not the files.
- ## Conclusion
  collapsed:: true
	- This brings us to the room's end. This room provided an introduction into firmware reverse engineering and allowed us to take a peak into how embedded file systems work.